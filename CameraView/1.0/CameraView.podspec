Pod::Spec.new do |s|

s.platform = :ios, '8.0'
s.ios.deployment_target = '8.0'
s.name = "CameraView"
s.summary = "Camera View allow use camera with a view from easy way"
s.requires_arc = true

s.version = "1.0"
s.license = { :type => "MIT", :file => "LICENSE" }

s.author = { "Joan Molinas" => "joanmramon@gmail.com" }
s.homepage = "https://github.com/ulidev/JMBackgroundCameraView"
s.source = { :git => "https://ulidev@bitbucket.org/ulidev/cameraview.git", :tag => s.version.to_s }
s.framework = "UIKit"
s.source_files = "CameraView/*.{h,m}"
end